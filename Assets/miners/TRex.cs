﻿using System.Diagnostics;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;
using System.Collections;
using System.Text;

public class TRex : MinerApp {

    public string Hashrate {
        get {
            string[] lines = running.StandardOutput.ReadToEnd().Split('\n', '\r');
            return lines[lines.Length - 1];
        }
    }

    public override void start(graphicsInstance gpu) {
        StringBuilder builder = new StringBuilder();
        //set the application path
        builder.Append("'");
        builder.Append(Application.persistentDataPath);
        builder.Append("/");
        builder.Append("trex");
        if (scr_main.platform == 1) {
            builder.Append(".exe");
        }
        builder.Append("' ");
        //miner account info
        builder.Append(" -o ");
        builder.Append(gpu.pool);
        builder.Append(" -u ");
        builder.Append(gpu.poolUsername);
        builder.Append(" -p ");
        builder.Append(gpu.poolPassword);
        //set the devices
        builder.Append(" -d ");
        builder.Append(gpu.id);
        if (gpu.slaveDevices.Count > 0) { 
            foreach (int i in gpu.slaveDevices) {
                builder.Append(",");
                builder.Append(i);
            }
        }
        //no-color is necessary, otherwise the escape characters have a field-day and break the parse
        builder.Append(" --no-color ");
        //miner additional settings
        if (gpu.poolWorker.Length > 1) {
            builder.Append(" --worker ");
            builder.Append(gpu.poolWorker);
        }
        builder.Append(" ");
        builder.Append(gpu.minerAuguments[0]);
        builder.Append(" --intensity ");
        builder.Append((int)(25f * (gpu.powerScale / 100f)));
        builder.Append(" --temperature-limit ");
        builder.Append(gpu.maxTemp);
        builder.Append(" --temperature-start ");
        builder.Append(gpu.maxTemp - gpu.cooldownTemp);


        running = scr_main.startProgram(builder.ToString());
        return;
    }

    public override IEnumerator downloadMiner(graphicsInstance gpu) {
#pragma warning disable CS0618 // Disable warning that type or member is obsolete. Sometimes old ways are best.
        //check if miner exists, and if not, download it.
        //todo version-specific paths
        if (!File.Exists(Application.persistentDataPath + "/t-rex/"+gpu.minerVersion)) {

            //change URL get index based on platform
            using (UnityWebRequest www = UnityWebRequest.Get(
            MinerApp.miners[gpu.getMinerVersionIndex()][scr_main.platform==0?2:3])) {
                yield return www.Send();

                if (www.isNetworkError || www.isHttpError) {
                        MonoBehaviour.print(www.error);
                }
                else if(scr_main.platform==0){
                    File.WriteAllBytes(Application.persistentDataPath + "/t-rex.tar.gz", www.downloadHandler.data);
                    Process extract = scr_main.startProgram(
                        "tar -xzf '" + Application.persistentDataPath 
                        +"/t-rex.tar.gz' --directory '" 
                        + Application.persistentDataPath + "/' t-rex/"+gpu.minerVersion);
                    extract.WaitForExit();
                    File.Delete(Application.persistentDataPath + "/t-rex.tar.gz");
                    MonoBehaviour.print("Download Complete");
                } else if (scr_main.platform == 1) {
                    File.WriteAllBytes(Application.persistentDataPath + "/t-rex.zip", www.downloadHandler.data);
                    //extraction license: http://infozip.sourceforge.net/license.html
                    //documentation: http://stahlworks.com/dev/index.php?tool=zipunzip#unzipexamp
                    //TODO: File needs to be in StreamingAssets folder
                    Process extract = scr_main.startProgram(
                        "unzip.exe '" + Application.streamingAssetsPath
                        + "/t-rex.zip'"
                        + Application.persistentDataPath + "/' t-rex");
                    extract.WaitForExit();
                    File.Delete(Application.persistentDataPath + "/t-rex.zip");
                    MonoBehaviour.print("Download Complete");
                }
            }
        }

        gpu.proc.start(gpu);
        MonoBehaviour.print("Miner Started");
    }

    /* output examples:
     * 
     *
        20210706 16:51:33 Mining at stratum.cudopool.com:30024, diff: 431.17 M
        20210706 16:51:33 GPU #0: Dell NVIDIA RTX 2060 - 18.54 MH/s, [T:67C, P:159W, F:86%, E:118kH/W], 7/7 R:0%
        20210706 16:51:33 Shares/min: 1.942 (Avr. 1.756)
        20210706 16:51:33 Uptime: 5 mins 1 sec | Algo: kawpow | T-Rex v0.19.11
        20210706 16:51:33 WD: 5 mins 4 secs, shares 7/7

     * 
     */

    public override bool parseWattage(string input, graphicsInstance gpu) {
        if (input.Contains("GPU #" + gpu.id + ": " + gpu.name)) {
            gpu.addTemp(scr_main.parseInt(Util.split(input, "T:")[1].Split('C')[0]));
            gpu.addWattage(scr_main.parseInt(Util.split(input, "P:")[1].Split('W')[0]));
            return true;
        }
        return false;
    }


    //updated from line: "20210415 18:39:14 [ OK ] 3/3 - 16.02 MH/s, 705ms ... GPU #0";
    //updates frequently. Only other data obtained from this is ping.
    public override bool ParseHashrate(string input, graphicsInstance gpu) {
        if (input.Contains(" [ OK ] ")) {
            gpu.addHashRate(scr_main.parseFloat(Util.split(input, " - ")[1].Split(' ')[0]));
            gpu.ping = scr_main.parseInt(Util.split(Util.split(input, ", ")[1], "ms")[0]);
            return true;
        }
        return false;
    }

    public override bool parseSharesAccepted(string input, graphicsInstance gpu) {
        if (input.Contains("shares:")) {
            gpu.shares = scr_main.parseInt(Util.split(input, "shares: ")[1].Split('/')[0]);
            gpu.stales = scr_main.parseInt(Util.split(input, "shares: ")[1].Split('/')[1]) - gpu.shares;
            return true;
        }
        return false;
    }

    //managed from line: 20210415 18:39:14 ethash epoch: 408, block: 12247720, diff: 4.29 G\n
    //updates regularly. Only other data obtained from this is block.
    //difficulty from line is skipped and instead done from the less frequent updates for better efficiency.
    public override bool parseEpoch(string input, graphicsInstance gpu) {
        if (input.Contains("epoch:")) {
            gpu.epoch = scr_main.parseInt(Util.split(input, "epoch: ")[1].Split(',')[0]);
            gpu.block = scr_main.parseInt(Util.split(input, "block: ")[1].Split(',')[0]);
            return true;
        }
        return false;
    }

    //managed from line: "Mining at stratum.cudopool.com:30004, diff: 4.29 G\n"
    //updates on a less frequent occasion
    public override bool parseDifficulty(string input, graphicsInstance gpu) {
        if (input.Contains("Mining at")) {
            gpu.poolDifficulty = Util.split(input, "diff: ")[1];
            return true;
        }
        return false;
    }


    //handled by #parseWattage
    public override bool parseTemp(string input, graphicsInstance gpu) { return false; }

    //handled from #parseSharesAccepted for better efficiency.
    public override bool parseSharesRejected(string input, graphicsInstance gpu) { return false; }

    //handled from #parseHashrate for better efficiency.
    public override bool parsePing(string input, graphicsInstance gpu) { return false; }


    //handled from #parseEpoch for better efficiency.
    public override bool parseBlockNumber(string input, graphicsInstance gpu) { return false; }

    UnityWebRequest www;
    public override IEnumerator parseAPIData(graphicsInstance gpu) {

        //todo use gpu.rpcPort instead of 4067
        //scr_main.println("getting data from " + "http://127.0.0.1:" + 4067 + "/summary");
        //download and parse miners data
        www = UnityWebRequest.Get(
        "http://127.0.0.1:"+ 4067 + "/summary");
        yield return www.SendWebRequest();
        if (!www.isNetworkError && !www.isHttpError) {
            string output = Encoding.UTF8.GetString(www.downloadHandler.data);

            output = "{\"accepted_count\":171,\"active_pool\":{\"difficulty\":\"179.45 M\",\"ping\":205,\"retries\":0,\"url\":\"stratum+tcp://etchash.na.mine.zergpool.com:9997\",\"user\":\"0x9436696Bbc6d6D8cCd3301e52EAAfB501574aA00\"},\"algorithm\":\"etchash\",\"api\":\"3.4\",\"build_date\":\"Feb 12 2021 20:53:35\",\"coin\":\"\",\"cuda\":\"11.10\",\"description\":\"T-Rex NVIDIA GPU miner\",\"difficulty\":0.044139999999999999,\"driver\":\"470.57.02\",\"gpu_total\":1,\"gpus\":[{\"dag_build_mode\":0,\"device_id\":0,\"efficiency\":\"158kH/W\",\"fan_speed\":90,\"gpu_id\":0,\"gpu_user_id\":0,\"hashrate\":15627651,\"hashrate_day\":0,\"hashrate_hour\":15170661,\"hashrate_instant\":15992805,\"hashrate_minute\":15633316,\"intensity\":10.0,\"low_load\":false,\"mtweak\":0,\"name\":\"NVIDIA RTX 2060\",\"pci_bus\":1,\"pci_domain\":0,\"pci_id\":0,\"potentially_unstable\":false,\"power\":95,\"power_avr\":99,\"temperature\":55,\"uuid\":\"9930be91d251aaffcf4a0f268c938210\",\"vendor\":\"Dell\"}],\"hashrate\":15627651,\"hashrate_day\":0,\"hashrate_hour\":15170661,\"hashrate_minute\":15633316,\"name\":\"t-rex\",\"os\":\"linux\",\"rejected_count\":0,\"revision\":\"9c70a63f2290\",\"sharerate\":4.6230000000000002,\"sharerate_average\":5.4269999999999996,\"solved_count\":0,\"stat_by_gpu\":[{\"accepted_count\":171,\"rejected_count\":0,\"solved_count\":0}],\"success\":1,\"ts\":1627239527,\"updates\":{\"download_status\":{\"downloaded_bytes\":0,\"last_error\":\"\",\"time_elapsed_sec\":0.0,\"total_bytes\":0,\"update_in_progress\":false,\"update_state\":\"idle\",\"url\":\"\"},\"md5sum\":\"5bec5f5f5aac0e0e2b3d53c6e0d1c4c8\",\"notes\":\"\\r\\n(autolykos2) performance improvements; bug fixes\",\"notes_full\":\"(autolykos2) performance improvements; bug fixes\",\"url\":\"https://github.com/trexminer/T-Rex/releases/download/0.21.4/t-rex-0.21.4-linux.tar.gz\",\"version\":\"0.21.4\"},\"uptime\":1905,\"version\":\"0.19.11\",\"watchdog_stat\":{\"built_in\":true,\"startup_ts\":1627237618967766,\"total_restarts\":0,\"uptime\":1908,\"wd_version\":\"0.19.11\"}}";
            scr_main.println(output);

            if (output.Contains("accepted_count")) {
                gpu.shares = int.Parse(Util.split(output, "accepted_count\":", ","));
            }
            if (output.Contains("rejected_count")) {
                gpu.stales = int.Parse(Util.split(output, "rejected_count\":", ","));
            }
            if (output.Contains("difficulty")) {
                gpu.poolDifficulty = Util.split(output, "difficulty\":\"", "\"");
            }
            if (output.Contains("ping")) {
                gpu.ping = int.Parse(Util.split(output, "ping\":\"", ","));
            }

            //parse data for this GPU
            string currentCard = Util.split(output, "\"device_id\":0")[1];

            if (output.Contains("hashrate")) {
                gpu.addHashRate(int.Parse(Util.split(currentCard, "hashrate\":\"", ",")));
            }
            if (output.Contains("temperature")) {
                gpu.addTemp(int.Parse(Util.split(currentCard, "temperature\":\"", ",")));
            }
            if (output.Contains("power")) {
                gpu.addWattage(int.Parse(Util.split(currentCard, "power\":\"", ",")));
            }

            //cover slave devices
            if (gpu.slaveDevices.Count > 0) {
                foreach (int slave in gpu.slaveDevices) {
                    currentCard = Util.split(output, "\"device_id\":"+slave)[1];
                    
                    if (output.Contains("hashrate")) {
                        scr_main.miners[slave].addHashRate(int.Parse(Util.split(currentCard, "hashrate\":\"", ",")));
                    }
                    if (output.Contains("temperature")) {
                        scr_main.miners[slave].addTemp(int.Parse(Util.split(currentCard, "temperature\":\"", ",")));
                    }
                    if (output.Contains("power")) {
                        scr_main.miners[slave].addWattage(int.Parse(Util.split(currentCard, "power\":\"", ",")));
                    }
                }
            }

            /*
             {"accepted_count":171,"active_pool":{"difficulty":"179.45 M","ping":205,"retries":0,
             "url":"stratum+tcp://etchash.na.mine.zergpool.com:9997","user":"0x9436696Bbc6d6D8cCd3301e52EAAfB501574aA00"},
             "algorithm":"etchash","api":"3.4","build_date":"Feb 12 2021 20:53:35","coin":"","cuda":"11.10",
             "description":"T-Rex NVIDIA GPU miner","difficulty":0.044139999999999999,"driver":"470.57.02",
             "gpu_total":1,"gpus":[{"dag_build_mode":0,"device_id":0,"efficiency":"158kH/W",
             "fan_speed":90,"gpu_id":0,"gpu_user_id":0,"hashrate":15627651,"hashrate_day":0,
             "hashrate_hour":15170661,"hashrate_instant":15992805,"hashrate_minute":15633316,
             "intensity":10.0,"low_load":false,"mtweak":0,"name":"NVIDIA RTX 2060","pci_bus":1,
             "pci_domain":0,"pci_id":0,"potentially_unstable":false,"power":95,"power_avr":99,
             "temperature":55,"uuid":"9930be91d251aaffcf4a0f268c938210","vendor":"Dell"}],
             "hashrate":15627651,"hashrate_day":0,"hashrate_hour":15170661,"hashrate_minute":15633316,
             "name":"t-rex","os":"linux","rejected_count":0,"revision":"9c70a63f2290",
             "sharerate":4.6230000000000002,"sharerate_average":5.4269999999999996,
             "solved_count":0,"stat_by_gpu":[{"accepted_count":171,"rejected_count":0,"solved_count":0}],
             "success":1,"ts":1627239527,"updates":{"download_status":{"downloaded_bytes":0,
             "last_error":"","time_elapsed_sec":0.0,"total_bytes":0,"update_in_progress":false,
             "update_state":"idle","url":""},"md5sum":"5bec5f5f5aac0e0e2b3d53c6e0d1c4c8",
             "notes":"\r\n(autolykos2) performance improvements; bug fixes",
             "notes_full":"(autolykos2) performance improvements; bug fixes",
             "url":"https://github.com/trexminer/T-Rex/releases/download/0.21.4/t-rex-0.21.4-linux.tar.gz",
             "version":"0.21.4"},"uptime":1905,"version":"0.19.11","watchdog_stat":{"built_in":true,
             "startup_ts":1627237618967766,"total_restarts":0,"uptime":1908,"wd_version":"0.19.11"}}
             */

        } else {
            //scr_main.println("failed to get data");
        }
    }

    /*
     * 
     * 
     */
}
