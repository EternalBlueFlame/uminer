﻿using System.Diagnostics;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;
using System.Collections;
using System.Text;

public class Phoenix : MinerApp {

    public string Hashrate {
        get {
            string[] lines = running.StandardOutput.ReadToEnd().Split('\n', '\r');
            return lines[lines.Length - 1];
        }
    }

    public override void start(graphicsInstance gpu) {
        StringBuilder builder = new StringBuilder();
        //set the application path
        builder.Append("'");
        builder.Append(Application.persistentDataPath);
        builder.Append("/");
        builder.Append("trex");
        if (scr_main.platform == 1) {
            builder.Append(".exe");
        }
        builder.Append("' ");
        //miner account info
        builder.Append(" -o ");
        builder.Append(gpu.pool);
        builder.Append(" -u ");
        builder.Append(gpu.poolUsername);
        builder.Append(" -p ");
        builder.Append(gpu.poolPassword);
        if (gpu.poolWorker.Length > 1) {
            builder.Append(" -worker ");
            builder.Append(gpu.poolWorker);
        }
        //set the devices
        builder.Append(" -d ");
        builder.Append(gpu.id);
        if (gpu.slaveDevices.Count > 0) { 
            foreach (int i in gpu.slaveDevices) {
                builder.Append(",");
                builder.Append(i);
            }
        }
        //no-color is necessary, otherwise the escape characters have a field-day and break the parse
        builder.Append(" --no-color ");
        //miner additional settings
        builder.Append(" ");
        builder.Append(gpu.minerAuguments[0]);
        builder.Append(" --intensity ");
        builder.Append((int)(25f * (gpu.powerScale / 100f)));
        builder.Append(" --temperature-limit ");
        builder.Append(gpu.maxTemp);
        builder.Append(" --temperature-start ");
        builder.Append(gpu.maxTemp - gpu.cooldownTemp);


        running = scr_main.startProgram(builder.ToString());
        return;
    }

    public override IEnumerator downloadMiner(graphicsInstance gpu) {
#pragma warning disable CS0618 // Disable warning that type or member is obsolete. Sometimes old ways are best.
        //check if miner exists, and if not, download it.
        //todo version-specific paths
        if (!File.Exists(Application.persistentDataPath + "/phoenix")) {

            //change URL get index based on platform
            using (UnityWebRequest www = UnityWebRequest.Get(
            MinerApp.miners[gpu.getMinerVersionIndex()][scr_main.platform==0?2:3])) {
                yield return www.Send();

                if (www.isNetworkError || www.isHttpError) {
                        MonoBehaviour.print(www.error);
                }
                else if(scr_main.platform==0){
                    File.WriteAllBytes(Application.persistentDataPath + "/phoenix.tar.gz", www.downloadHandler.data);
                    Process extract = scr_main.startProgram(
                        "tar -xzf '" + Application.persistentDataPath 
                        + "/phoenix.tar.gz' --directory '"
                        + Application.persistentDataPath + "/' phoenix");
                    extract.WaitForExit();
                    File.Delete(Application.persistentDataPath + "/phoenix.tar.gz");
                    MonoBehaviour.print("Download Complete");
                } else if (scr_main.platform == 1) {
                    File.WriteAllBytes(Application.persistentDataPath + "/t-rex.zip", www.downloadHandler.data);
                    //extraction license: http://infozip.sourceforge.net/license.html
                    //documentation: http://stahlworks.com/dev/index.php?tool=zipunzip#unzipexamp
                    //TODO: File needs to be in StreamingAssets folder
                    Process extract = scr_main.startProgram(
                        "unzip.exe '" + Application.streamingAssetsPath
                        + "/phoenix.zip'"
                        + Application.persistentDataPath + "/' phoenix");
                    extract.WaitForExit();
                    File.Delete(Application.persistentDataPath + "/phoenix.zip");
                    MonoBehaviour.print("Download Complete");
                }
            }
        }

        gpu.proc.start(gpu);
        MonoBehaviour.print("Miner Started");
    }


    //updated from line: "20210415 18:39:14 [ OK ] 3/3 - 16.02 MH/s, 705ms ... GPU #0";
    //updates frequently. Only other data obtained from this is ping.
    public override bool ParseHashrate(string input, graphicsInstance gpu) {
        if (input.Contains(" [ OK ] ")) {
            gpu.addHashRate(scr_main.parseFloat(Util.split(input, " - ")[1].Split(' ')[0]));
            gpu.ping = scr_main.parseInt(Util.split(Util.split(input, ", ")[1], "ms")[0]);
            return true;
        }
        return false;
    }

    public override bool parseSharesAccepted(string input, graphicsInstance gpu) {
        if (input.Contains("shares:")) {
            gpu.shares = scr_main.parseInt(Util.split(input, "shares: ")[1].Split('/')[0]);
            gpu.stales = scr_main.parseInt(Util.split(input, "shares: ")[1].Split('/')[1]) - gpu.shares;
            return true;
        }
        return false;
    }

    //managed from line: 20210415 18:39:14 ethash epoch: 408, block: 12247720, diff: 4.29 G\n
    //updates regularly. Only other data obtained from this is block.
    //difficulty from line is skipped and instead done from the less frequent updates for better efficiency.
    public override bool parseEpoch(string input, graphicsInstance gpu) {
        if (input.Contains("epoch:")) {
            gpu.epoch = scr_main.parseInt(Util.split(input, "epoch: ")[1].Split(',')[0]);
            gpu.block = scr_main.parseInt(Util.split(input, "block: ")[1].Split(',')[0]);
            return true;
        }
        return false;
    }

    //managed from line: "Mining at stratum.cudopool.com:30004, diff: 4.29 G\n"
    //updates on a less frequent occasion
    public override bool parseDifficulty(string input, graphicsInstance gpu) {
        if (input.Contains("Mining at")) {
            gpu.poolDifficulty = Util.split(input, "diff: ")[1];
            return true;
        }
        return false;
    }



    //handled from #parseSharesAccepted for better efficiency.
    public override bool parseSharesRejected(string input, graphicsInstance gpu) { return false; }

    //handled from #parseHashrate for better efficiency.
    public override bool parsePing(string input, graphicsInstance gpu) { return false; }


    //handled from #parseEpoch for better efficiency.
    public override bool parseBlockNumber(string input, graphicsInstance gpu) { return false; }

    public override bool parseTemp(string input, graphicsInstance gpu) {
        throw new System.NotImplementedException();
    }

    public override bool parseWattage(string input, graphicsInstance gpu) {
        throw new System.NotImplementedException();
    }

    public override IEnumerator parseAPIData(graphicsInstance gpu) {
        throw new System.NotImplementedException();
    }
}
