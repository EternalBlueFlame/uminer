using System;
using static CryptoAttribute;

public class Pools {


    public static string POOL_ID_CUDO = "cudo";
    public static string getCudoAddress = "stratum+tcp://stratum.cudopool.com";
    public static string[] getCudoCPUAlgos() { return SYMBOLS(new[] { Crypto.XMR }); }

    public static string[] getCudoGPUAlgos() { return SYMBOLS(new[] { 
    Crypto.BTC, Crypto.ETH, Crypto.RVN, Crypto.ETC, Crypto.MTP }); }

    public static string getCudoPortForAlgo(string algo) {
        if (algo.Equals(SYMBOLS(Crypto.XMR))) {
            return "30010";
        } else if(algo.Equals(SYMBOLS(Crypto.BTC))) {
            return "30000";
        } else if (algo.Equals(SYMBOLS(Crypto.BTG))) {
            return "30001";
        } else if (algo.Equals(SYMBOLS(Crypto.ETH))) {
            return "30004";
        } else if(algo.Equals(SYMBOLS(Crypto.RVN))) {
            return "30024";
        } else if(algo.Equals(SYMBOLS(Crypto.ETC))) {
            return "30005";
        } else if (algo.Equals(SYMBOLS(Crypto.MTP))) {
            return "30019";
        }

        return "-1";//this is needed for compiler, but we should never actually need to use it.
    }



    public static string POOL_ID_ZERG = "zergpool";





    public static string[] listPools = { POOL_ID_CUDO, POOL_ID_ZERG };



}

internal class CryptoAttribute : Attribute {
    public string Symbol;
    public string Name;

    [System.Flags]
    public enum Crypto {
        [Crypto(Symbol = "BTC", Name = "Bitcoin")]
        BTC = 0x0,
        [Crypto(Symbol = "XMR", Name = "Monero")]
        XMR = 0x1,
        [Crypto(Symbol = "ETH", Name = "Etherium")]
        ETH = 0x2,
        [Crypto(Symbol = "ETC", Name = "Etherium Classic")]
        ETC = 0x3,
        [Crypto(Symbol = "RVN", Name = "Ravencoin")]
        RVN = 0x4,
        [Crypto(Symbol = "MTP", Name = "zCoin")]
        MTP = 0x5,
        [Crypto(Symbol = "BTG", Name = "Bitcoin Gold")]
        BTG = 0x6
    }


    public static string SYMBOLS(Crypto s) {
        return ((CryptoAttribute)s.GetType().GetCustomAttributesData()).Name;
    }
    public static string[] SYMBOLS(Crypto[] s) {
        string[] val = new string[s.Length];
        for (int i = 0; i <= s.Length; i++) {
            val[i] = SYMBOLS(s[i]);
        }
        return val;
    }

    public static string NAMES(Crypto s) {
        return ((CryptoAttribute)s.GetType().GetCustomAttributesData()).Name;
    }
    public static string[] NAMES(Crypto[] s) {
        string[] val = new string[s.Length];
        for (int i = 0; i <= s.Length; i++) {
            val[i] = NAMES(s[i]);
        }
        return val;
    }
}