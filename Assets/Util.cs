﻿using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class Util : MonoBehaviour {

    public static string[] nullString = new string[] { };

	/* unused in this project, but a holdover for convenience, will probably find a use here.
	  public static void renderSliders(graphicsInstance device){
			float x = 0.15f;
			float y = 3;

			resources[i].sliderValue = Mathf.RoundToInt(
				GUI.HorizontalSlider(scaledRect(x, (y * 0.06f), 0.25f, 0.05f), resources[i].sliderValue, 0, resources[i].sliderValue + getSliderUse(resources))
			);
			GUI.Label(scaledRect(x, (y * 0.06f) + 0.025f, 0.25f, 0.5f), getNameByIndex(i) +": "+resources[i].sliderValue + "%", GUIGame.CoreStyle);
	}*/


    //shorthand for making a rect scaled to the screen dimensions.
	public static Rect scaledRect(float x, float y, float width, float height){
		return new Rect(Screen.width * x, Screen.height * y, Screen.width * width, Screen.height * height);
	}

    public static string[] split(string input, string regex) {
        return input.Split(new[] { regex }, System.StringSplitOptions.None);
    }

    public static string split(string input, string regexStart, string regexEnd) {
        return input.Split(new[] { regexStart }, System.StringSplitOptions.None)[1]
        .Split(new[] { regexEnd }, System.StringSplitOptions.None)[0];
    }

    public IEnumerator getHTTP(string address) {
        //send web request and wait for return value, should be near-instant
        UnityWebRequest www = UnityWebRequest.Get(address);

        yield return www.SendWebRequest();

        //return the string from HTTP, or null if error.
        if (!www.isNetworkError && !www.isHttpError) {
            yield return Encoding.UTF8.GetString(www.downloadHandler.data);
        }else {
            yield return null;
        }
    }


    public class AsyncData {
        public Coroutine coroutine { get; private set; }
        public string result;
        private IEnumerator target;

        public AsyncData(MonoBehaviour m, IEnumerator t) {
            target = t;
            coroutine = m.StartCoroutine(Run());
        }

        private IEnumerator Run() {
            while (target.MoveNext()) {
                if(typeof(string).IsInstanceOfType(target.Current)){
                    result = (string)target.Current;
                }
                yield return result;
            }
        }
    }
}

