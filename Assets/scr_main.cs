﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine.Networking;
using System.Text;
using System;

public class scr_main : MonoBehaviour {


    public static int menu=1;
    int language = 0;
    public static int platform = -1;
    //in theory this should be fully redundant, but I'd rather be safe.
    public static bool initComplete = false, running =false;

    public static scr_main INSTANCE;

    public static List<graphicsInstance> miners = new List<graphicsInstance>();

    Process sysInfo;
    

    //TODO: make access to this through a function that redirects through the translator.
    //TODO: split this into an array for each card, and/or better yet put it in graphicsInstance
    public static string eventlog = "";


    // Use this for initialization
    void Start () {
        INSTANCE = this;
        StartCoroutine(DownloadMinerData());

        initCards();


        StartCoroutine(tickEvent());
    }



    // Update is called once per frame
    void Update () {}

    public static IEnumerator DownloadMinerData() {

        //download and parse miners data
        UnityWebRequest www = UnityWebRequest.Get(
        "https://gitlab.com/EternalBlueFlame/ucmm/-/raw/master/Assets/miners/miners.txt");
        yield return www.SendWebRequest();
        if (!www.isNetworkError && !www.isHttpError) {
            foreach (string s in Encoding.UTF8.GetString(www.downloadHandler.data).Split('\n')) {
                if (!s.StartsWith("#") && !s.StartsWith("<")) {
                    MinerApp.miners.Add(s.Split(','));
                }
            }
        }
    }



    public void initCards() {
        //check and cache platform
        if(Application.platform == RuntimePlatform.LinuxPlayer || Application.platform == RuntimePlatform.LinuxEditor) {
            platform = 0;
        } else if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor) {
            platform = 1;
        }


        //todo

        //windows: wmic path win32_VideoController get name
        //gets the list of graphics adapters by name, in order, only displays the name:
        //NVIDIA GeForce RTX 2060

        //linux: lspci | grep VGA
        //returns a similar value, formatted different:
        //01:00.0 VGA compatible controller: NVIDIA Corporation TU106 [GeForce RTX 2060 Rev. A] (rev a1)



        //run an external bash process to get the output of listing the hardware.
        sysInfo = startProgram("nvidia-smi -L");

        //add data to log and also debug
        string[] values = sysInfo.StandardOutput.ReadToEnd().Split(new char[] { '\n', '\r' });

        foreach (string s in values) {
            if (s.Contains("GPU")) {
                graphicsInstance gpu = new graphicsInstance();
                gpu.proc = new TRex();
                gpu.id = int.Parse(s.Split(':')[0].Split(' ')[1]);
                gpu.name = s.Split(':')[1].Split('(')[0];
                gpu.manufacturer = 0;
                //gpu.maxWattage = parseFloat(getNvidiaSMIValue("power.max_limit", gpu.id).Split(']')[1].Split(' ')[0]);
                //automatically add additional GPU's to GPU 0
                if (miners.Count > 0) {
                    gpu.isSlave = true;
                    gpu.pool = 0+"";
                    miners[0].slaveDevices.Add(gpu.id);
                }
                gpu.rpcPort = 31337 + gpu.id;
                miners.Add(gpu);
            }
        }

        sysInfo.WaitForExit();


        //This is a fake GPU entry for testing purposes. DO NOT let this be uncommented for release builds       
        graphicsInstance testgpu = new graphicsInstance();
        testgpu.proc = new TRex();
        testgpu.id = int.Parse("1");
        testgpu.name = "Nvidia Test master";
        testgpu.manufacturer = 0;
        miners.Add(testgpu);

        graphicsInstance testgpu2 = new graphicsInstance();
        testgpu2.proc = new TRex();
        testgpu2.id = int.Parse("2");
        testgpu2.name = "Nvidia Test Slave 2";
        testgpu2.manufacturer = 0;
        miners.Add(testgpu2);

        graphicsInstance testgpu3 = new graphicsInstance();
        testgpu3.proc = new TRex();
        testgpu3.id = int.Parse("3");
        testgpu3.name = "Nvidia Test Slave 3";
        testgpu3.manufacturer = 0;
        miners.Add(testgpu3);

        graphicsInstance testgpu4 = new graphicsInstance();
        testgpu4.proc = new TRex();
        testgpu4.id = int.Parse("4");
        testgpu4.name = "Nvidia Test Slave 4";
        testgpu4.manufacturer = 0;
        miners.Add(testgpu4);


        graphicsInstance testgpu5 = new graphicsInstance();
        testgpu5.proc = new TRex();
        testgpu5.id = int.Parse("5");
        testgpu5.name = "Nvidia Test Slave 5";
        testgpu5.manufacturer = 0;
        miners.Add(testgpu5);


        graphicsInstance testgpu6 = new graphicsInstance();
        testgpu6.proc = new TRex();
        testgpu6.id = int.Parse("6");
        testgpu6.name = "Nvidia Test Slave 6";
        testgpu6.manufacturer = 0;
        miners.Add(testgpu6);


        graphicsInstance testgpu7 = new graphicsInstance();
        testgpu7.proc = new TRex();
        testgpu7.id = int.Parse("7");
        testgpu7.name = "Nvidia Test Slave 7";
        testgpu7.manufacturer = 0;
        miners.Add(testgpu7);
        initComplete = true;
    }


    //update during the 'physics' tick.
    IEnumerator tickEvent() {
        while (true) {
            //pause logging card info 
            if (!initComplete) {
                yield return new WaitForSeconds(5f);
            }
            eventlog = "";


            foreach (graphicsInstance gpu in miners) {
                eventlog += "Card " + gpu.id + ": " + gpu.name + "\n";

                //println("Card " + gpu.id + ": " + gpu.name + gpu.isSlave);
                if (!gpu.isSlave) {

                    StartCoroutine(gpu.proc.parseAPIData(gpu));
                }

                //add current temp and clamp list count to a minute (one entry every 5 seconds, 5*12=60)
                //gpu.addMemoryTemp(parseFloat(getNvidiaSMIValue("temperature.memory", 0).Split('y')[1].Split(' ')[0]));
                //todo: add more in-depth features for things like memory temp after finding a way around AMD's garbage API


                if (running) {
                    List<string> lines = gpu.readLines();
                    foreach (string log in lines) {
                        /*if (!gpu.proc.ParseHashrate(log, gpu)) {
                            if (!gpu.proc.parseSharesAccepted(log, gpu)) {
                                if (!gpu.proc.parseSharesRejected(log, gpu)) {
                                    if (!gpu.proc.parsePing(log, gpu)) {
                                        if (!gpu.proc.parseEpoch(log, gpu)) {
                                            if (!gpu.proc.parseBlockNumber(log, gpu)) {
                                                if (!gpu.proc.parseDifficulty(log, gpu)) {
                                                    if (!gpu.proc.parseWattage(log, gpu)) {
                                                        if (!gpu.proc.parseTemp(log, gpu)) {
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }*/
                    }
                }

                //add temp and average to output string
                /*eventlog += 
                "temp: " + gpu.temp[gpu.temp.Count-1] + 
                " Average: " + gpu.getAverageTemp() + 
                "\n Hashrate: " + gpu.getHashrate() +
                " - Average: " + gpu.getAverageHashrateRecent() +
                " - Total Average: " + gpu.getAverageHashrate() +
                "\n Shares - Accepted: " + gpu.shares +
                " - Rejected: " + gpu.stales +
                "\n Wattage: " + gpu.getWattage() +
                " - Average: " + gpu.getAverageWattageRecent() +
                " - Total Average: " + gpu.getAverageWattage() +
                "\n Pool epoch: " + gpu.epoch +
                " - Block: " + gpu.block +
                " - Ping: " + gpu.ping + "ms" +
                "\n\n";*/
            }


            //wait for 5 seconds before letting the loop continue
            yield return new WaitForSeconds(5f);
        }
    }


    /*
    //generic value for getting SMI data from nvidia for a specific card
    public static string getNvidiaSMIValue(string value, int gpuID) {
        return getCommandValue("nvidia-smi --query-gpu="+ value +" --id=" + gpuID + " --format=csv");
    }

    public static string getRcomSMIValue(string value, int gpuID) {
        if (platform == 0) {
            //gets data from system bus in linux folders.
            //see: https://wiki.archlinux.org/title/AMDGPU#Overclocking
            //this should work for nvidia as well.
            getCommandValue("cat /sys/class/drm/card" + gpuID + "/device/hwmon/hwmon");
        } else if (platform == 1) {
            //todo
            //get data using the AMD ADL SDK
            return "";
        }
    }*/

    //this allows parsing floats with invalid values, in case the card does not support a feature
    public static float parseFloat(string s) {
        float.TryParse(s, out float output);
        return output;
    }
    public static int parseInt(string s) {
        int.TryParse(s, out int output);
        return output;
    }


    /**
     * Translation
     * uses the language internal variable to decide language.
     * uses the internalName augument to define what is being translated
     * 
     * in the case of english, the returns value is the same as the input, because the internal names are english.
     * TODO: later on if this gets massive it might not hurt to make it a class of it's own and trim out the clutter.
     */
    string TranslateName(string internalName){
        switch(language){
            //TODO: other languages here
            default:{//english and fallback
                switch(internalName){
                    default:{return internalName;}
                }
            }
        }
    }


    //start and return a daemon for an external provcess
    public static Process startProgram(string program) {
        //cover running an external application for linux
        if (platform==0) {
            ProcessStartInfo startInfo = new ProcessStartInfo() {
                FileName = "/bin/bash",
                UseShellExecute = false,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                StandardOutputEncoding = Encoding.UTF8,
                CreateNoWindow = true,
                Arguments = " -c \"" + program + " \""

            };
            Process p = new Process {
                StartInfo = startInfo
            };
            p.Start();
            return p;
        }


        //cover running an external application for Windows
        if (platform==1) {
            ProcessStartInfo startInfo = new ProcessStartInfo() {
                FileName = "C:\\Windows\\system32\\cmd.exe",
                UseShellExecute = false,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                StandardOutputEncoding = Encoding.UTF8,
                WindowStyle = ProcessWindowStyle.Hidden,//this might be unnecessary, but i don't trust windows.
                CreateNoWindow = true,
                Arguments = program
            };
            Process p = new Process {
                StartInfo = startInfo
            };
            p.Start();
            return p;
        }


        //good luck figuring this out for other platforms.
        return null;
    }

    //inefficient but a lot less to write elsewhere.
    public static string getCommandValue(string program) {
        string result;
        Process p = startProgram(program);
        result = p.StandardOutput.ReadToEnd();
        p.WaitForExit();
        return result;
    }

    public static void println(string s) {
        print(s);
    }

}
