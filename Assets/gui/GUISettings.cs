﻿using UnityEngine;
using System.Collections;

public class GUISettings: MonoBehaviour {
    private static Vector2 scrollView = new Vector2(0, 0);

    private static int overlayGPU = -1;
    private static int overlayID = -1;

    static Vector2 scrollbar = new Vector2(0, 0);
    public static void settingsMenu() {
        //define the device config menus
        Rect r = GUIGame.scaledRect(0.45f, 0.3f, 0.5f, 0.7f);
        GUILayout.BeginArea(r);
        GUILayout.BeginHorizontal();
        r = GUIGame.scaledRect(0f, 0f, 0.2f, 0.7f);
        GUILayoutOption[] guiOptions = new GUILayoutOption[] {
            GUILayout.MaxWidth(r.width),
            GUILayout.MaxHeight(r.height)
        };

        GUILayoutOption[] o = new GUILayoutOption[]{
            GUILayout.Height(Screen.height*0.03f),
        };
        GUILayout.BeginScrollView(scrollbar, GUIStyle.none, GUIStyle.none, guiOptions);

        foreach (graphicsInstance gpu in scr_main.miners) {
            GUILayout.Label(" ");
            GUILayout.Label("GPU: " + gpu.id + " : " + gpu.name);
            GUI.backgroundColor = GUIGame.buttonColor();
            if (!gpu.isSlave) {
                GUILayout.Label("Device pool address", o);
                PoolConfigs.addPoolConfigs(true, o, gpu);
            } else {
                GUILayout.Label("Paired Device/Pool ", o);
            }
        }

        GUILayout.EndScrollView();


        r = GUIGame.scaledRect(0, 0f, 0.2f, 0.7f);
        guiOptions = new GUILayoutOption[] {
            GUILayout.MaxWidth(r.width),
            GUILayout.MaxHeight(r.height)
        };
        scrollbar = GUILayout.BeginScrollView(scrollbar, guiOptions);
        foreach (graphicsInstance gpu in scr_main.miners) {
            GUILayout.Label(" ");
            GUILayout.Label(" ");
            if (!gpu.isSlave) {
                if (GUILayout.Button(gpu.pool.Equals("null") ? "no pool selected" : gpu.pool, o)) {
                    setOverlay(gpu.id, 1);
                }

                PoolConfigs.addPoolConfigs(false, o, gpu);
            } else {
                if (GUILayout.Button("Paired with GPU: " + gpu.pool, o)) {
                    setOverlay(gpu.id, 1);
                }
            }
        }
        GUILayout.EndScrollView();
        GUILayout.EndHorizontal();
        GUILayout.EndArea();

        if (overlayID ==1) {
            poolMenu();
        } else if (overlayID==2) {
            minerMenu();
        }

    }


    //for the pool selection overlay menu
    private static void poolMenu() {
        GUILayoutOption[] o = new GUILayoutOption[]{
            GUILayout.Height(Screen.height*0.03f),
        };
        GUIGame.popupOverlay();
        //draw actual menu
        GUILayout.BeginArea(GUIGame.scaledRect(0.35f, 0.3f, 0.5f, 0.6f));
        GUILayoutOption[] guiOptions = GUIGame.setLayoutOptions(0.5f, 0.6f);
        scrollbar2 = GUILayout.BeginScrollView(scrollbar2, guiOptions);
        GUI.backgroundColor = GUIGame.buttonColor();
        string ogPool = scr_main.miners[overlayGPU].pool;
        scr_main.miners[overlayGPU].pool = GUILayout.TextField(scr_main.miners[overlayGPU].pool, o);

        if (GUILayout.Button("Close Menu", o)) {
            //if it was a slave device, and that has changed, be sure to clean it off the original device
            if (scr_main.miners[overlayGPU].isSlave &&
                ogPool != scr_main.miners[overlayGPU].pool) {
                scr_main.miners[scr_main.parseInt(ogPool)]
                    .slaveDevices.Remove(overlayGPU);
                scr_main.miners[overlayGPU].isSlave = false;
            }
            setOverlay(-1, -1);
        }
        //add known pools
        foreach (string pool in Pools.listPools) {
            if (GUILayout.Button(pool, o)) {
                //be sure if it is a slave device, to remove it from the master.
                if (scr_main.miners[overlayGPU].isSlave) {
                    scr_main.miners[scr_main.parseInt(scr_main.miners[overlayGPU].pool)]
                        .slaveDevices.Remove(overlayGPU);
                    scr_main.miners[overlayGPU].isSlave = false;
                }
                PoolConfigs.setPool(pool, scr_main.miners[overlayGPU]);
                setOverlay(-1, -1);
            }
        }
        //add other cards
        foreach (graphicsInstance pool in scr_main.miners) {
            if (pool.id != overlayGPU && !pool.isSlave) {
                if (GUILayout.Button("Pair with device " + pool.id + ", " + pool.name, o)) {
                    scr_main.miners[overlayGPU].pool = "" + pool.id;
                    scr_main.miners[overlayGPU].isSlave = true;
                    scr_main.miners[pool.id].slaveDevices.Add(overlayGPU);
                    setOverlay(-1, -1);
                }
            }
        }

        GUILayout.EndScrollView();
        GUILayout.EndArea();
    }

    private static Vector2 scrollbar2 = new Vector2(0, 0);
    //for the miner application selection overlay menu
    private static void minerMenu() {

        GUILayoutOption[] o = new GUILayoutOption[]{
           GUILayout.Height(Screen.height*0.03f),
        };
        GUIGame.popupOverlay();

        //draw actual menu
        GUILayout.BeginArea(GUIGame.scaledRect(0.35f, 0.3f, 0.5f, 0.6f));
        GUILayoutOption[] guiOptions = GUIGame.setLayoutOptions(0.5f, 0.6f);
        scrollbar2 = GUILayout.BeginScrollView(scrollbar2, guiOptions);
        GUI.backgroundColor = GUIGame.buttonColor();
        if (GUILayout.Button("Close Menu", o)) {
            setOverlay(-1, -1);
        }
        //add supported miner tools
        foreach (string[] pool in MinerApp.miners) {
            if (GUILayout.Button(pool[2], o)) {
                //manufacturer check limits to nvidia
                if (pool[0].ToLower().Contains("trex") && scr_main.miners[overlayGPU].manufacturer==0) { 
                    scr_main.miners[overlayGPU].proc = new TRex();
                    scr_main.miners[overlayGPU].minerVersion = pool[0];

                }

                if (pool[0].ToLower().Contains("phoenix")) {
                    scr_main.miners[overlayGPU].proc = new Phoenix();
                    scr_main.miners[overlayGPU].minerVersion = pool[0];

                }
                setOverlay(-1,-1);
            }
        }

        GUILayout.EndScrollView();
        GUILayout.EndArea();
    }

    //shorthand for setting the state of the overlay screen, -1 for disabled
    public static void setOverlay(int gpuID, int overlay) {
        overlayGPU = gpuID;
        overlayID = overlay;

    }
}
