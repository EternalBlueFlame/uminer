using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class PoolConfigs : MonoBehaviour
{

    public static void addPoolConfigs(bool nameSide, GUILayoutOption[] o, graphicsInstance gpu) {

        if (gpu.pool.Equals(Pools.POOL_ID_CUDO)) {
            if (nameSide) {
                GUILayout.Label("Device pool Organization", o);
                GUILayout.Label("Device pool WorkerName", o);
            } else {
                string org = GUILayout.TextField(gpu.minerAuguments[1], o);
                string worker = GUILayout.TextField(gpu.poolWorker, o);
                //password is ALWAYS lowercase x
                gpu.poolPassword = "x";
                //use field 1 as trickery to check if the username has changed, and in turn update from remote
                if (org != gpu.minerAuguments[1] || gpu.poolWorker!=worker) {
                    gpu.minerAuguments[1] = org;
                    gpu.poolWorker = worker;
                    //the coroutine will get the orgID from the API and then append the worker
                    scr_main.INSTANCE.StartCoroutine(downloadCudoUsername(gpu));
                }
                //field 2 is disabled here
                 gpu.minerAuguments[2] = "";

            }

        } else if (gpu.pool.Equals(Pools.POOL_ID_ZERG)) {
            if (nameSide) {
                GUILayout.Label("Device pool wallet", o);
                GUILayout.Label("Device pool exchange crypto", o);
                GUILayout.Label("Device pool party ID", o);
            } else {
                gpu.poolUsername = GUILayout.TextField(gpu.poolUsername, o);
                gpu.minerAuguments[1] = GUILayout.TextField(gpu.minerAuguments[1], o);
                gpu.minerAuguments[2] = GUILayout.TextField(gpu.minerAuguments[2], o);
                //zerg handles fancy stuff as part of the password, so update that using the above field values
                gpu.poolPassword =
                (gpu.minerAuguments[1].Length > 0 ? "c=" + gpu.minerAuguments[1] : "")+//add payout crypto
                (gpu.minerAuguments[1].Length > 0 && gpu.minerAuguments[2].Length > 0?",":"")+//more than 1 augument, add comma
                (gpu.minerAuguments[2].Length > 0 ? "m=party."+gpu.minerAuguments[2] : "");//party ID

            }
        } else { //cover user-defined pools
            if (nameSide) { 
                GUILayout.Label("Device pool wallet", o);
                GUILayout.Label("Device pool password", o);
                GUILayout.Label("Device pool worker", o);
            } else {
                gpu.poolUsername = GUILayout.TextField(gpu.poolUsername, o);
                gpu.poolPassword = GUILayout.TextField(gpu.poolPassword, o);
                gpu.poolWorker = GUILayout.TextField(gpu.poolWorker, o);
            }

        }
        advancedOptions(nameSide, o, gpu);
    }


    public static void poolGenerics(bool nameSide, GUILayoutOption[] o, graphicsInstance gpu) {
        if (nameSide) {

            GUILayout.Label((int)(25f * (gpu.powerScale / 100f)) + " - " + gpu.powerScale + "%", o);
        } else {

            //todo: this should be more dynamic, 40 is the same as intensity 10, which is t-rex minimum, but some can go lower
            gpu.powerScale = Mathf.RoundToInt(
                GUILayout.HorizontalSlider(gpu.powerScale, 40, 100, o)
            );

        }
    }


    public static void advancedOptions(bool nameSide, GUILayoutOption[] o, graphicsInstance gpu) {
        if (nameSide) {
            if (gpu.expertMode) {
                GUILayout.Label("Device max temperature  (Celcius)", o);
                GUILayout.Label("Device cooldown before restart (Celcius)", o);
                GUILayout.Label("Additional miner auguments", o);
                GUILayout.Label("Advanced Options.", o);
            } else {
                GUILayout.Label("Advanced Options. (ENABLING MAY HARM DEVICE)", o);
            }
        } else {
            if (gpu.expertMode) {
                gpu.maxTemp = int.Parse(GUILayout.TextField(gpu.maxTemp + "", o));
                gpu.cooldownTemp = int.Parse(GUILayout.TextField(gpu.cooldownTemp + "", o));
                gpu.minerAuguments[0] = GUILayout.TextField(gpu.minerAuguments[0], o);
                if (GUILayout.Button("Enabled", o)) {
                    gpu.expertMode = !gpu.expertMode;
                }
            } else {
                if (GUILayout.Button("Disabled", o)) {
                    gpu.expertMode = !gpu.expertMode;
                }
            }
        }

    }

    public static void setPool(string pool, graphicsInstance gpu) {

        //reset pool settings
        gpu.poolPassword = "";
        gpu.poolUsername = "";
        gpu.poolWorker = "";
        gpu.minerAuguments = new string[]{ "", "", ""};

        //cudo ALWAYS has password as a lowercase x with no exceptions, so we might as well pre-fill that.
        if (pool.Equals(Pools.POOL_ID_CUDO)) {
            gpu.poolPassword = "x";
        }

        gpu.pool = pool;

    }


    public static IEnumerator downloadCudoUsername(graphicsInstance gpu) {

        //download and parse miners data
        UnityWebRequest www = UnityWebRequest.Get(
        "https://api.cudo.org/v1/organizations/byslug?slug="+gpu.minerAuguments[1]);
        yield return www.SendWebRequest();
        if (!www.isNetworkError && !www.isHttpError) {
            string payload = Encoding.UTF8.GetString(www.downloadHandler.data);
            if (payload.Contains("\"id\"")) {
                //use ordinal to be sure system culture doesn't break it
                gpu.poolUsername = "o:"+ 
                payload.Substring(payload.IndexOf(":", System.StringComparison.Ordinal) + 1,
                payload.IndexOf(",", System.StringComparison.Ordinal))
                +":n:"+gpu.poolWorker;
            }
        }
    }
}
