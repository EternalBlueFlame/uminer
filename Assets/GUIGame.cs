﻿using UnityEngine;
using System.Collections;
using System;

public class GUIGame: MonoBehaviour {

    public static GUIStyle CoreStyle = new GUIStyle();
    public static bool darkMode = true;
    public Texture subCard;

    //colors
    public static Color backgroundColor() { return darkMode?new Color(18,18,18):new Color(255,255,255); }
    public static Color primaryColor() { return darkMode ? new Color(29,29,29) : new Color(244,244,244); }
    public static Color secondaryColor() { return darkMode ? new Color(35,35,35) : new Color(220,220,220); }
    public static Color shadeColor() { return darkMode ? new Color(255,255,255,15) : new Color(0,0,0,15); }
    public static Color buttonColor() { return darkMode ? new Color(44,44,44) : new Color(211,211,211); }
    public static Color fontColor() { return darkMode ? new Color(226, 226, 226) : new Color(29, 29, 29); }
    public static Color fontHoverColor() { return darkMode ? new Color(220, 220, 220) : new Color(35, 35, 35); }

    static Vector2 scrollView = new Vector2(0, 0);
    static Vector3 mousePos = new Vector3(0,0,0);
    static int inputCooldown = 0;

    void Start() {
        QualitySettings.vSyncCount = 0;
        CoreStyle.alignment = TextAnchor.UpperLeft;
        CoreStyle.fontSize = 16;
        CoreStyle.normal.textColor = fontColor();
        CoreStyle.active.textColor = fontColor();
        CoreStyle.hover.textColor = fontHoverColor();CoreStyle.richText = false;
        GUI.backgroundColor = backgroundColor();

        guiOptions = new GUILayoutOption[] {
            GUILayout.MaxWidth(r.width),
            GUILayout.MaxHeight(r.height)
        };

        InvokeRepeating("invokedUpdate", 1,0.2f);
    }

    void invokedUpdate() {
        if (inputCooldown < 1) {
            Application.targetFrameRate = 1;
        } else {
            inputCooldown--;
            Application.targetFrameRate = 15;

            guiOptions = new GUILayoutOption[] {
                GUILayout.MaxWidth(r.width),
                GUILayout.MaxHeight(r.height)
            };
        }

        //reset timer when there's an input, 15 is roughly around 5 seconds
        if(mousePos!= Input.mousePosition) {
            mousePos = Input.mousePosition;
            inputCooldown = 30;
        } else if (Input.anyKeyDown) {
            inputCooldown = 30;
        }


    }

    void OnGUI() {

        //show screen parts that are shared
        sidebar();

        //show screens specific to the current menu
        switch (scr_main.menu) {
            case 1: {mainMenu(); break;}
            case 2: {GUISettings.settingsMenu(); break; }
            case 5: {creditsMenu(); break; }

        }

    }

    public void sidebar() {

        //add a GUI button to the side for settings.
        //what it displays is defined by a terinary statement that checks if we are on it's menu or not.
        GUI.backgroundColor = new Color(255,0,0);

        if (GUI.Button(scaledRect(0.025f, 0.2f, 0.2f, 0.1f), scr_main.running ? "Stop" : "Start")) {
            foreach (graphicsInstance gpu in scr_main.miners) {
                if (!scr_main.running) {
                    StartCoroutine(gpu.proc.downloadMiner(gpu));
                } else {
                    gpu.proc.running.Kill();
                    gpu.proc.running.WaitForExit();
                    gpu.proc.running = null;
                }
            }
            scr_main.running = !scr_main.running;
        }
        if (GUI.Button(scaledRect(0.025f, 0.35f, 0.2f, 0.1f), scr_main.menu!=2?"Settings":"Mining Monitor")) {
            scr_main.menu=scr_main.menu!=2?2:1;
        }
        if (GUI.Button(scaledRect(0.025f, 0.65f, 0.2f, 0.1f), "Credits/License")) {
            scr_main.menu = 5;
        }
    }

    public void topBar() {

    }

    //init and hold variables for faster processing
    private int rows, listX;
    private float yMain, listY;
    private Rect r;
    private GUILayoutOption[] guiOptions;
    //draw the main menu
    public void mainMenu() {

        GUI.backgroundColor = new Color(128, 128, 128);
        //Define an area, then draw the background box in it.
        //GUILayout.BeginArea(scaledRect(0.35f, 0.3f, 0.6f, 0.6f));
        //GUILayout.Box("", GUILayout.Width(Screen.width * 0.6f), GUILayout.Height(Screen.height * 0.65f));
        //GUILayout.EndArea();

        //Define another area, which will be on top of previous due to order of execution, then add the text to it
        GUILayout.BeginArea(scaledRect(0.25f, 0.15f, 0.7f, 0.85f));


        r = scaledRect(0, 0, 0.7f, 0.8f);
        scrollView = GUILayout.BeginScrollView(scrollView, guiOptions);

        rows = (int)(((Screen.width * 0.55f) - 255) / 240);
        listX = 0; listY = 0; yMain = 0;
        foreach(graphicsInstance gpu in scr_main.miners) {
            if (!gpu.isSlave) {

                //calculate the main box background stretch
                if (gpu.slaveDevices != null && gpu.slaveDevices.Count > 1) {
                    foreach (int slave in gpu.slaveDevices) {
                        listX++;
                        if (listX >= rows) {
                            listX = 0;
                            listY+=listY<1?0.5f:1;//the first one is half height
                        }
                    }
                }
                //render the main gpu
                cardInfoBox(gpu, 0, yMain, listY, r.height);
                listX = 0; listY = 0;
                //render all the slave devices
                if (gpu.slaveDevices != null && gpu.slaveDevices.Count > 0) {
                    foreach (int slave in gpu.slaveDevices) {
                        if (listX >= rows) {
                            listX = 0;
                            yMain+=0.7f;
                        }
                        graphicsInstance card = scr_main.miners[slave];
                        cardInfoBox(card, listX, yMain, 0, r.height);
                        listX++;
                    }
                }

                yMain++;
            }
        }
        //forces scroll view size
        GUILayout.Box("", GUILayout.Width(0.1f), GUILayout.Height((220 * yMain)+10));



        //todo: find a way to get stuff like bar graphs into the view, needs event log split per-card
        GUILayout.EndScrollView();
        GUILayout.EndArea();
    }


    public void creditsMenu() {

        GUI.backgroundColor = new Color(128, 128, 128);
        //Define another area, which will be on top of previous due to order of execution, then add the text to it
        GUILayout.BeginArea(scaledRect(0.25f, 0.15f, 0.7f, 0.85f));

        r = scaledRect(0, 0, 0.7f, 0.8f);
        GUILayoutOption[] guiOptions = new GUILayoutOption[] {
            GUILayout.MaxWidth(r.width),
            GUILayout.MaxHeight(r.height)
        };
        scrollView = GUILayout.BeginScrollView(scrollView, guiOptions);

        GUILayout.Label("Development: EternalBlueFlame");
        GUILayout.Label("");
        GUILayout.Label("We claim no affiliation with any miners or pools.");
        GUILayout.Label("All we claim to provide is a GUI management tool for them.");
        GUILayout.Label("By using this software you accept sole liability for ");
        GUILayout.Label("any and all damage or loss, whether from direct or indirect use.");
        GUILayout.Label("");
        GUILayout.Label("Each miner binary is property of their individual developers, and they take sole responsibility.");
        GUILayout.Label("");
        GUILayout.Label("T-Rex miner provided by: http://trex-miner.com");
        GUILayout.Label("Documentation: https://github.com/trexminer/T-Rex");
        GUILayout.Label("Phoenix miner provided by: https://bitcointalk.org/index.php?action=profile;u=1522040");
        GUILayout.Label("check their post history for links and such.");
        GUILayout.Label("");
        GUILayout.Label("We use a custom binary for extracting archives called infozip, their have their own license here:");
        GUILayout.Label("http://infozip.sourceforge.net/license.html");
        GUILayout.Label("more information on infozip here:");
        GUILayout.Label("http://stahlworks.com/dev/index.php?tool=zipunzip#unzipexamp");
        GUILayout.Label("");
        GUILayout.Label("");
    


        GUILayout.EndScrollView();
        GUILayout.EndArea();
    }


    //todo make small widget things for each GPU, and then sort them on the main menu.
    //note master cards intentionally dont end the area so there's room for the slave cards.
    void cardInfoBox(graphicsInstance gpu, float x, float y, float height, float layoutHeight) {
        if (gpu.isSlave) {
            if (!isInRect(layoutHeight,(220 * y) + 15)) {
                //return;
            }
            GUI.DrawTexture(new Rect(445 + (240 * x), (220 * y) + 15, 240, 150), subCard, ScaleMode.StretchToFill);
            GUILayout.BeginArea(new Rect(465 + (240 * x), (220 * y) + 40, 240, 150), CoreStyle);
            GUILayout.Label(gpu.name, CoreStyle);
            GUILayout.Label("Suceeded shares: " + gpu.shares, CoreStyle);
            GUILayout.Label("Failed shares: " + gpu.stales, CoreStyle);
            GUILayout.Label("hashrate: " + gpu.getHashrate() + " - Average: " + gpu.getAverageHashrate(), CoreStyle);
            GUILayout.Label("tempature: " + gpu.getTemp() + "c - Average: " + gpu.getAverageTemp() +"c", CoreStyle);
            GUILayout.Label("wattage: " + gpu.getWattage() + "w - Average: " +gpu.getAverageWattage()+ "w", CoreStyle);
            GUILayout.EndArea();
        } else {
            if (!isInRect(layoutHeight, (220 * y) + 10)) {
                return;
            }
            //math.max the height to be sure it fits the text when there's only 1 row
            StartArea(new Rect(10, (220 * y)+10, (Screen.width * 0.7f)-20, (150*height)+200), CoreStyle,null);
            GUILayout.Label(gpu.name, CoreStyle);
            GUILayout.Label("Worker: " + gpu.poolUsername, CoreStyle);
            GUILayout.Label("pool: " + gpu.pool, CoreStyle);
            GUILayout.Label("ping: " + gpu.ping+" ms", CoreStyle);
            GUILayout.Label("epoch: "+gpu.epoch, CoreStyle);
            GUILayout.Label("Block: "+gpu.block, CoreStyle);
            GUILayout.Label("shares: " + gpu.shares + " - Errored shares: " + gpu.stales, CoreStyle);
            GUILayout.Label("hashrate: " + gpu.getHashrate() + " - Average: " + gpu.getAverageHashrate(), CoreStyle);
            GUILayout.Label("tempature: " + gpu.getTemp() + "c - Average: " + gpu.getAverageTemp() + "c", CoreStyle);
            GUILayout.Label("wattage: " + gpu.getWattage() + "w - Average: " + gpu.getAverageWattage() + "w", CoreStyle);
            GUILayout.EndArea();
        }


    }

    //todo: make another overlay menu for algo selection
    //todo: make algo selection automatic/locked/limited based on the pool address[1] value.


    /*
    * --------------------
    * Shorthands    
    * -------------------- 
    */

    //shorthand for starting an area, also draws the background rect
    public static void StartArea(Rect r, GUIStyle s, Texture t) {
        if (s == null) {
            if (t != null) {
                GUILayout.BeginArea(r, t);
            } else {
                GUILayout.BeginArea(r);
            }
        }
        else {
            if (t != null) {
                GUILayout.BeginArea(r, t, s);
            }
            else {
                GUILayout.BeginArea(r,s);
            }
        }

        GUILayout.Box("", GUILayout.Width(r.width+10), GUILayout.Height(r.height+10));
        GUILayout.EndArea();

        //add padding
        r = new Rect(r.x + 10, r.y + 10, r.width + 10, r.height + 10);

        GUILayout.BeginArea(r);
    }

    //shorthand for making a rect scaled to the screen dimensions.
    public static Rect scaledRect(float x, float y, float width, float height) {
        return new Rect(Screen.width * x, Screen.height * y, Screen.width * width, Screen.height * height);
    }

    //shorthand for making the layout options for individual menu entries
    public static GUILayoutOption[] setLayoutOptions(float width,float height) {
        return new GUILayoutOption[] {
                GUILayout.MaxWidth(Screen.width * width),
                GUILayout.MaxHeight(Screen.height *height)
            };
    }

    //draws the grey-out of the menu
    public static void popupOverlay() {
        //draw background overlay
        GUILayout.BeginArea(scaledRect(0f, 0f, 1f, 1f));
        GUI.backgroundColor = shadeColor();
        GUILayout.Box("", setLayoutOptions(1,1));
        GUILayout.EndArea();
    }


    public static Boolean isInRect(float boxHeight, float itemPos) {
        return !(itemPos-scrollView.y < -200 || itemPos-scrollView.y > boxHeight);
    }

}
