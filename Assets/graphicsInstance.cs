﻿using System.Diagnostics;
using System.Collections.Generic;

public class graphicsInstance {
    //todo Could this be confortably re-used for CPU?

    public MinerApp proc;
    public List<float> temp = new List<float>();
    //public List<float> memoryTemp = new List<float>();
    public List<float> wattage = new List<float>();
    //public float maxWattage = 0;
    public int id=-1;
    public List<int> slaveDevices = new List<int>();
    public bool isSlave = false;
    public string name;
    public string minerVersion = "null";
    public int rpcPort;

    //mining pool options
    public List<float> hashrate = new List<float>();
    public string poolDifficulty = "";
    public int shares = 0;
    public int stales = 0;
    public int ping = 0;
    public int epoch = 0;
    public int block = 0;
    public string pool = Pools.POOL_ID_CUDO;
    public string poolWorker = "UMiner";
    public string poolUsername = "o:119601:n:UMiner";
    public string poolPassword = "x";
    public string[] minerAuguments = {"", "eternalblueflame", "eternalblueflame" };

    public int maxTemp=60;
    public int cooldownTemp = 10;
    public bool expertMode = false;


    //mining settings
    public int powerScale=40;

    //0 for nvidia, 1 for AMD.
    //todo: intel GPU's for 2.
    public int manufacturer;

    public graphicsInstance setSlave(bool b) {
        isSlave = b;
        return this;
    }

    public List<string> readLines() {
        List<string> str = new List<string>();
        if (proc!=null && proc.running!=null) {
            while (proc.running.StandardOutput.Peek() > -1) {
                str.Add(proc.running.StandardOutput.ReadLine());
            }
        }
        return str;
    }

    public int getMinerVersionIndex() {
        for (int i = 0, maxLength = MinerApp.miners.Count; i < maxLength; i+=4) {
            if (MinerApp.miners[i][0].Equals(minerVersion)) {
                return i;
            }
        }
        //this shouldn't be possible.
        return 0;
    }

    public float getAverageTemp() {
        if (temp.Count == 0) {
            return 0;
        }
        float tempAverage = 0;
        foreach (float t in temp) {
            tempAverage += t;
        }
        tempAverage /= temp.Count;
        return tempAverage;
    }

    public void addTemp(float t) {
        temp.Add(t);
        if (temp.Count > 12) {
            temp.RemoveRange(0, 1);
        }
    }

    public void addMemoryTemp(float t) {
        /*memoryTemp.Add(t);
        if (memoryTemp.Count > 12) {
            memoryTemp.RemoveRange(0, 1);
        }*/
    }

    public float getTemp() {
        return temp.Count > 0 ? temp[temp.Count - 1] : 0;
    }

    public void addHashRate(float t) {
        hashrate.Insert(0,t);
    }

    public float getAverageHashrate() {
        if (hashrate.Count == 0) {
            return 0;
        }
        float hashAverage = 0;
        foreach (float h in hashrate) {
            hashAverage += h;
        }
        hashAverage /= hashrate.Count;
        return hashAverage;
    }

    //because hash is added backwards we can count from the bottom up for more recent
    public float getAverageHashrateRecent() {
        if (hashrate.Count == 0) {
            return 0;
        }
        float hashAverage = 0;
        int i = 0;
        foreach(float h in hashrate) {
            if (i >= 20) { break; }
            hashAverage += h;
            i++;
        }
        hashAverage /= System.Math.Min(hashrate.Count,20);
        return hashAverage;
    }

    public float getHashrate() {
        return hashrate.Count>0?hashrate[hashrate.Count-1]:0;
    }


    public float getWattage() {
        return wattage.Count > 0 ? wattage[wattage.Count - 1] : 0;
    }
    public float getAverageWattage() {
        if (wattage.Count == 0) {
            return 0;
        }
        float wattavg = 0;
        foreach (float t in wattage) {
            wattavg += t;
        }
        wattavg /= temp.Count;
        return wattavg;
    }

    public void addWattage(int t) {
        wattage.Add(t);
        if (wattage.Count > 12) {
            wattage.RemoveRange(0, 1);
        }
    }

    //because hash is added backwards we can count from the bottom up for more recent
    public float getAverageWattageRecent() {
        if (wattage.Count == 0) {
            return 0;
        }
        float wattAverage = 0;
        int i = 0;
        foreach (float h in wattage) {
            if (i >= 20) { break; }
            wattAverage += h;
            i++;
        }
        wattAverage /= System.Math.Min(wattage.Count, 20);
        return wattAverage;
    }
}

