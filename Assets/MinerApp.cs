﻿using System.Diagnostics;
using System.Collections;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System;
using UnityEngine;

public abstract class MinerApp {

    public Process running =null;
    public int minerIndex = -1;
    private Socket socket;
    private NetworkStream stream;
    private StreamReader reader;
    private StreamWriter writer;

    public static List<string[]> miners = new List<string[]>();

    public abstract bool ParseHashrate(string input, graphicsInstance gpu);
    public abstract bool parseSharesAccepted(string input, graphicsInstance gpu);
    public abstract bool parseSharesRejected(string input, graphicsInstance gpu);
    public abstract bool parsePing(string input, graphicsInstance gpu);

    public abstract bool parseEpoch(string input, graphicsInstance gpu);
    public abstract bool parseBlockNumber(string input, graphicsInstance gpu);
    public abstract bool parseDifficulty(string input, graphicsInstance gpu);

    public abstract bool parseTemp(string input, graphicsInstance gpu);
    public abstract bool parseWattage(string input, graphicsInstance gpu);

    public abstract void start(graphicsInstance gpu);

    public abstract IEnumerator downloadMiner(graphicsInstance gpu);


    public abstract IEnumerator parseAPIData(graphicsInstance gpu);

    //NOTE: always returns null on first check to give time for connection.
    //will return null if disconnected and reconnecting.
    public string getClaymoreAPIValue(string val, int rpcPort) {
        //if not connected, setup the connection
        if (socket == null || !socket.Connected) {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(new IPEndPoint(IPAddress.Parse("127.0.0.1"), rpcPort));

            stream = new NetworkStream(socket);
            writer = new StreamWriter(stream);
            reader = new StreamReader(stream);

            //if call to close stream, do that.
        } else if (val.Equals("close")) {
            reader.Close();
            socket.Close();
            scr_main.println("stream closed");
            return null;

            //if connected, and data is available, get and print data from stream.
        } else if (stream.DataAvailable && !reader.EndOfStream) {
            return reader.ReadLine();
        }

        //if connected and no data available, send the value to request data.
        writer.WriteLine("{\"id\":0,\"jsonrpc\":\"2.0\",\"method\":\"" + val + "\"}");
        writer.Flush();

        return null;
    }

}
